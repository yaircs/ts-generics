//----------------------------
//      every challenge
//----------------------------
// implement the `every` function
// add types and generics as needed
const students: Array<string> = ["jOHn", "DAn", "ruTH", "jAnE", "beN"];

const hasN = (str: string): boolean => str.toLowerCase().includes("n");
const min3Chars = (str: string): boolean => str.length >= 3;

const ages: Array<number> = [72, 16, 22, 42, 36, 11, 52];
const over21 = (n: number) => n > 21;

//implement the `every` function
const every = <T>(items: T[], cb: (item: T) => boolean ): boolean => {
    for (let item of items) {
        if(!cb(item)) {
            return false;
        }
    }
    return true;
};

const all_students_have_n  = every(students, hasN); // type inference
const all_students_min_3 = every(students, min3Chars); // type inference
const all_ages_over_21 = every(ages, over21); // type inference

console.log({ all_students_have_n }); // -> false
console.log({ all_students_min_3 }); // -> true
console.log({ all_ages_over_21 }); // -> false
